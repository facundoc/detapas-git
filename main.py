# vim: set fileencoding=utf-8 :
"""DeTapas
Servidor de imagenes de libros
"""
import json

import cloudstorage
from PIL import Image
import webapp2


with open('settings.json') as i:
    settings = json.load(i)


class DeTapasIndexHandler(webapp2.RequestHandler):

    def get(self):
        self.response.write('_.: DeTapas :._')


class DeTapasApiCallMixin(webapp2.RequestHandler):

    def validate_api_key(self, api_key, **kwargs):
        authorized_keys = settings['authorized_clients']

        if api_key not in authorized_keys:
            self.abort(403)

        self.client_config = authorized_keys[api_key]


class DeTapasGetImageHandler(DeTapasApiCallMixin):

    def split_target_name(self, target_name):
        """Recibe el nombre completo para la imagen buscada
            y devuelve por separado el id del producto, el ancho y el alto
            suponiendo que ancho y alto estan separados por 'x'
            y admitiendo que se omitan el ancho o el alto

            >>> DeTapasGetImageHandler.split_target_name('1235.jpg')
            ('1235', 0, 0)

            >>> DeTapasGetImageHandler.split_target_name('1235x320.png')
            ('1235', 320, 0)

            >>> DeTapasGetImageHandler.split_target_name('1235x320x200')
            ('1235', 320, 200)
        """
        target_parts = target_name.split('.')[0].split('x')
        product_id = target_parts[0]
        try:
            width = int(target_parts[1]) if len(target_parts) > 1 else 0
        except ValueError:
            width = None

        try:
            height = int(target_parts[2]) if len(target_parts) > 2 else 0
        except ValueError:
            height = None

        return product_id, width, height

    def resize_and_get(self, product_id, width, height):
        """Busca la imagen para el producto indicado
            la redimensiona de acuerdo al ancho y alto pedidos
            la guarda en Cloud Storage
            y devuelve el nombre usado para guardarla
        """

        # buscar la imagen original
        try:
            cloudstorage.stat('/%s/%s' % (settings['bucket_name'], product_id))
        except cloudstorage.NotFoundError:
            return self.client_config['default_image']

        with cloudstorage.open('/%s/%s' % (settings['bucket_name'], product_id)) as i:
            img = Image.open(i)

            # determinar el nuevo tamaño y redimensionar la imagen
            original_size = img.size
            proportion = 1
            if width != 0 and height == 0:
                proportion = float(width) / original_size[0]
            elif width == 0 and height != 0:
                proportion = float(height) / original_size[1]
            elif width != 0 and height != 0:
                proportion = float(height) / original_size[1]
                # TODO: por ahora cuando estan los dos uso el alto, deberia tomar el que se ajuste mejor
#                if original_size[1] * proportion > height:
#                    porportion = float(height) / original_size[1]

            new_size = [int(x * proportion) for x in original_size]

            resized_img = img.resize(new_size, Image.ANTIALIAS)

            # guardar la imagen redimensionada
            new_name = '%sx%sx%s.jpg' % (product_id, new_size[0], new_size[1])
            with cloudstorage.open('/%s/%s' % (settings['bucket_name'], new_name), 'w',
                                   content_type='image/jpg') as o:
                resized_img.save(o, 'JPEG')

        # devovler la url de la imagen redimensionada
        return new_name

    def get(self, api_key, target_name):
        self.validate_api_key(api_key)

        try:
            cloudstorage.stat('/%s/%s' % (settings['bucket_name'], target_name))
        except cloudstorage.NotFoundError:
            product_id, width, height = self.split_target_name(target_name)
            target_name = self.resize_and_get(product_id, width, height)

        base_url = '%(storage_url)s%(bucket_name)s/' % settings
        destination_url = base_url + target_name

        self.response.write(destination_url)

        return self.redirect(str(destination_url))


class DeTapasGetImageInfoHandler(DeTapasApiCallMixin):

    def get(self, api_key, target_name):
        self.validate_api_key(api_key)

        try:
            image_info = cloudstorage.stat('/%s/%s' % (settings['bucket_name'], target_name))
        except cloudstorage.NotFoundError:
            self.abort(404)

        response = {'bytes': image_info.st_size, 'md5': image_info.etag}
        self.response.write(json.dumps(response))


class DeTapasLoadImageHandler(DeTapasApiCallMixin):

    def get(self, api_key):
        self.validate_api_key(api_key)

        self.response.write('<html><head><title>DeTapas</title></head>'
                            '<body><form method="post" action="#" enctype="multipart/form-data">'
                            '   <input type="text" name="product_id"/>'
                            '   <input type="file" name="data"/>'
                            '   <input type="submit" name="submit" value="Subir"/>'
                            '</form></body>'
                            '</html>')

    def post(self, api_key):
        self.validate_api_key(api_key)

        product_id = self.request.get('product_id')
        data = self.request.get('data')

        bucket_name = settings['bucket_name']
        filename = '/%s/%s' % (bucket_name, product_id)
        with cloudstorage.open(filename, 'w', content_type='image/jpg') as o:
            o.write(data)

        self.response.write('<html><body><h1>_(o.-)_d</h1>')
        self.response.write('<p>%s</p>' % cloudstorage.stat(filename))
        self.response.write('</body></html>')


DeTapasApp = webapp2.WSGIApplication([
    webapp2.Route('/', DeTapasIndexHandler, name='index'),
    webapp2.Route(r'/<api_key:\w+>/code/<target_name:.+>', DeTapasGetImageHandler, name='image'),
    webapp2.Route(r'/<api_key:\w+>/info/<target_name:.+>', DeTapasGetImageInfoHandler, name='image_info'),
    webapp2.Route(r'/<api_key:\w+>/put/', DeTapasLoadImageHandler, name='upload'),
], debug=True)
