#!/bin/sh
# Preparar el entorno para ejecutar los tests

# Instalar las dependencias
pip install -r requirements.txt

# Bajar y descomprimir el SDK de App Engine
if [ ! -d google_appengine ]
then
    wget --no-verbose https://storage.googleapis.com/appengine-sdks/featured/google_appengine_1.9.13.zip
    unzip -q google_appengine_1.9.13.zip
fi
