#!/usr/bin/env python
# vim: set fileencoding=utf-8 :
"""Tests para el proyecto De Tapas
"""
import json
import traceback
import unittest

#TODO: configurar bien el entorno para no tener que hacer esto
import os
import sys
lib_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'lib')
sys.path.insert(0, lib_path)

import cloudstorage
import mock
import webapp2

import main


class SiteTestCase(unittest.TestCase):

    def setUp(self):
        settings_patch = mock.patch.object(main, 'settings', new=self.make_testings_settings())
        settings_patch.start()
        self.addCleanup(settings_patch.stop)

        cloudstorage_patch = mock.patch.object(main, 'cloudstorage', spec=True)
        cloudstorage_patch.start()
        self.addCleanup(cloudstorage_patch.stop)

        image_patch = mock.patch.object(main, 'Image', spec=True)
        image_patch.start()
        self.addCleanup(image_patch.stop)

    def make_testings_settings(self):
        "Genera un diccionarion con la configuracion usada para los tests"
        return {'storage_url': 'http://st.io/', 'bucket_name': 'b',
                'authorized_clients': {'1235': {'default_image': 'nocover.jpg'}}}

    def get_product_image(self, client_key, product_id, width=None, height=None):
        "Busca la imagen para un producto"
        url = '/%s/code/%s' % (client_key, product_id)
        if width is not None:
            url += 'x%s' % width

        if height is not None:
            url += 'x%s' % height

        webrequest = webapp2.Request.blank(url)
        webresponse = webrequest.get_response(main.DeTapasApp)

        return webresponse

    def test_unauthorized_keys(self):
        "Solo se puede acceder a una tapa con la clave valida"

        # dada una clave de cliente invalida
        client_key = 'noexiste'
        assert client_key not in main.settings['authorized_clients']

        # dado un codigo de producto valido
        product_id = '271'

        # cuando se intenta acceder a la imagen del producto
        url = '/%s/code/%s.jpg' % (client_key, product_id)
        webrequest = webapp2.Request.blank(url)
        webresponse = webrequest.get_response(main.DeTapasApp)

        # se debe recibir una respuesta con codigo 403
        self.assertEqual(403, webresponse.status_int)

    def test_product_not_found(self):
        "Si el codigo de producto no existe se debe devolver una respuesta que redirija a un default"

        # dada una clave de cliente valida
        client_key = '1235'
        assert client_key in main.settings['authorized_clients']

        # dado un codigo de producto inexistente
        product_id = '828182846'

        def fake_stat(target_name):
            if product_id in target_name:
                raise cloudstorage.NotFoundError()

        main.cloudstorage.stat = fake_stat
        main.cloudstorage.NotFoundError = cloudstorage.NotFoundError

        # cuando se intenta acceder a la imagen del producto
        webresponse = self.get_product_image(client_key, product_id)

        # se debe recibir una respuesta que redirija a una imagen default
        base_url = '%(storage_url)s%(bucket_name)s' % main.settings
        default_image = main.settings['authorized_clients'][client_key]['default_image']

        self.assertEqual(302, webresponse.status_int)
        self.assertEqual('/'.join((base_url, default_image)), webresponse.headers['Location'])

    def test_valid_key_and_product_code(self):
        "Si la clave es valida y el producto tiene imagen se debe redirigir a la URL del storage"

        # dada una clave de cliente valida
        client_key = '1235'
        assert client_key in main.settings['authorized_clients']

        # dado un codigo de producto existente
        product_id = '12358'

        # cuando se accede a la imagen del producto
        webresponse = self.get_product_image(client_key, product_id)

        # se debe recibir una respuesta que redirija al storage
        self.assertEqual(302, webresponse.status_int)

    def test_resized_width(self):
        "Si se pide una imagen con un ancho especifico se debe redirigir a la URL del storage"

        # dada una clave de cliente valida
        client_key = '1235'

        # dado un codigo de producto existente
        product_id = '12358'

        def fake_stat(target_name):
            if '%sx' % product_id in target_name:
                raise cloudstorage.NotFoundError()

        main.cloudstorage.stat = fake_stat
        main.cloudstorage.NotFoundError = cloudstorage.NotFoundError

        main.Image.open().size = (1280, 1024)

        # cuando se accede a la imagen del producto
        webresponse = self.get_product_image(client_key, product_id, 64)

        # se debe haber redimensionado la imagen
        self.assertEqual(1, main.Image.open().resize.call_count)

        # se debe recibir una respuesta que redirija al storage de la imagen redimensionada
        self.assertEqual(302, webresponse.status_int)
        self.assertTrue('12358x64' in webresponse.headers['Location'])

    def test_resized_width_and_height(self):
        "Si se pide una imagen con un ancho y alto especificos se debe redirigir a la URL del storage"

        # dada una clave de cliente valida
        client_key = '1235'

        # dado un codigo de producto existente
        product_id = '12358'

        def fake_stat(target_name):
            if '%sx' % product_id in target_name:
                raise cloudstorage.NotFoundError()

        main.cloudstorage.stat = fake_stat
        main.cloudstorage.NotFoundError = cloudstorage.NotFoundError

        main.Image.open().size = (1280, 1024)

        # cuando se accede a la imagen del producto
        webresponse = self.get_product_image(client_key, product_id, 64, 128)

        # se debe haber redimensionado la imagen
        self.assertEqual(1, main.Image.open().resize.call_count)

        # se debe recibir una respuesta que redirija al storage de la imagen redimensionada
        self.assertEqual(302, webresponse.status_int)
        self.assertTrue('x128' in webresponse.headers['Location'])

    def test_resize_only_once(self):
        "Si se pide dos veces la misma imagen con el mismo tamaño solo se debe redimesionar una vez"

        # dada una clave de cliente valida
        client_key = '1235'

        # dado un codigo de producto existente
        product_id = '12358'

        def fake_stat(target_name):
            if '%sx' % product_id in target_name:
                raise cloudstorage.NotFoundError()

        main.cloudstorage.stat = fake_stat
        main.cloudstorage.NotFoundError = cloudstorage.NotFoundError

        main.Image.open().size = (1280, 1024)

        # cuando se accede a la imagen del producto
        webresponse = self.get_product_image(client_key, product_id, 64, 128)

        # se debe haber redimensionado la imagen
        self.assertEqual(1, main.Image.open().resize.call_count)

        # se debe haber guardado la imagen redimensionada en el storage
        save_call = main.cloudstorage.open.call_args_list[1]
        self.assertEqual('w', save_call[0][1])

        # se debe recibir una respuesta que redirija al storage de la imagen redimensionada
        self.assertEqual(302, webresponse.status_int)

        main.cloudstorage.stat = mock.Mock()

        # cuando se accede por segunda vez  a la imagen del producto
        webresponse = self.get_product_image(client_key, product_id, 64, 128)

        # no se debe haber hecho la redimension
        self.assertEqual(1, main.Image.open().resize.call_count)

        # se debe recibir una respuesta que redirija al storage de la imagen redimensionada
        webresponse = self.get_product_image(client_key, product_id, 64, 128)

    def test_keep_aspect_ratio(self):
        "Al redimensionar se debe mantener la relacion alto ancho de la imagen"

        # dada una clave de cliente valida
        client_key = '1235'

        # dado un codigo de producto existente
        product_id = '12358'

        def fake_stat(target_name):
            if '%sx' % product_id in target_name:
                raise cloudstorage.NotFoundError()

        main.cloudstorage.stat = fake_stat
        main.cloudstorage.NotFoundError = cloudstorage.NotFoundError

        original_size = (800, 600)
        main.Image.open().size = original_size

        for width in (400, 800, 1000):
            # cuando se pide la imagen del producto con un ancho determinado
            webresponse = self.get_product_image(client_key, product_id, width)

            # se debe haber redimensionado manteniendo la relacion alto/ancho
            resize_args = main.Image.open().resize.call_args[0]
            self.assertEqual(float(original_size[0]) / original_size[1],
                             float(resize_args[0][0]) / resize_args[0][1])

            # se debe recibir una respuesta que redirija al storage de la imagen redimensionada
            self.assertEqual(302, webresponse.status_int)
            self.assertTrue('%sx%s' % (product_id, width) in webresponse.headers['Location'])

    def test_product_image_info_invalid_key(self):
        "Solo pueden buscar informacion de una imagen los clientes autorizados"

        # dada una clave de cliente invalida
        client_key = 'noexiste'

        # dado un codigo de producto existente
        product_id = '12358'

        # cuando se pide la informacion para la imagen del producto
        url = '/%s/info/%s.jpg' % (client_key, product_id)
        webrequest = webapp2.Request.blank(url)
        webresponse = webrequest.get_response(main.DeTapasApp)

        # se debe recibir una respuesta con codigo 403
        self.assertEqual(403, webresponse.status_int)

    def test_product_image_info_not_found(self):
        "Si se pide la informacion de la imagen de un producto que no existe se debe recibir un 404"

        # dada una clave de cliente valida
        client_key = '1235'

        # dado un codigo de producto inexistente
        product_id = '000'

        def fake_stat(target_name):
            raise cloudstorage.NotFoundError()

        main.cloudstorage.stat = fake_stat
        main.cloudstorage.NotFoundError = cloudstorage.NotFoundError

        # cuando se pide la informacion para la imagen del producto
        url = '/%s/info/%s.jpg' % (client_key, product_id)
        webrequest = webapp2.Request.blank(url)
        webresponse = webrequest.get_response(main.DeTapasApp)

        # se debe recibir una respuesta con codigo 404
        self.assertEqual(404, webresponse.status_int)

    def test_product_image_info(self):
        "Se debe poder pedir la informacion de la imagen para un producto existente"

        # dada una clave de cliente valida
        client_key = '1235'

        # dado un codigo de producto existente
        product_id = '12358'

        main.cloudstorage.stat.return_value.st_size = 4096
        main.cloudstorage.stat.return_value.etag = 'f266cd8e42e38872e495a2bb3c8dcb42'

        # cuando se pide la informacion para la imagen del producto
        url = '/%s/info/%s.jpg' % (client_key, product_id)
        webrequest = webapp2.Request.blank(url)
        webresponse = webrequest.get_response(main.DeTapasApp)

        # se debe recibir una respuesta con codigo 200
        self.assertEqual(200, webresponse.status_int)

        # se debe recibir una respuesta con un JSON valido
        try:
            response_data = json.loads(webresponse.text)
        except:
            self.fail(traceback.format_exc())

        # se debe reicibr el tamaño de la imagen en la respuesta
        self.assertEqual(4096, response_data['bytes'])

        # se debe reciibr el MD5 de la imagen en la respuesta
        self.assertEqual('f266cd8e42e38872e495a2bb3c8dcb42', response_data['md5'])


if __name__ == '__main__':
    unittest.main()
