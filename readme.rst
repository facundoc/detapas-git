========
De Tapas
========

Servidor de imagenes

Codeship
--------

.. image :: https://www.codeship.io/projects/acc7be30-379b-0132-7520-1a638ec7e56e/status?branch=master
    :target: https://www.codeship.io/projects/41763


Drone.io
--------

.. image :: https://drone.io/bitbucket.org/facundoc/detapas-git/status.png
   :target: https://drone.io/bitbucket.org/facundoc/detapas-git
